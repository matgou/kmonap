CREATE EXTENSION "uuid-ossp";

CREATE TABLE CONFIGURATION(
   ID    VARCHAR(100)    NOT NULL,
   NAME  VARCHAR(20)     NOT NULL,
   VALUE VARCHAR(255)    NOT NULL,
   PRIMARY KEY (ID)
);

INSERT INTO CONFIGURATION (ID, NAME, VALUE) VALUES
(uuid_generate_v4(), 'site_name', 'SampleApp');
(uuid_generate_v4(), 'site_description', 'Application de démonstration');

CREATE TABLE NOTE(
   ID    VARCHAR(100)    NOT NULL,
   TXT  VARCHAR(255)    NOT NULL,
   PRIMARY KEY (ID)
);

INSERT INTO NOTE (ID, TXT) VALUES
(uuid_generate_v4(), 'Verifier pneu'),
(uuid_generate_v4(), 'Verifier niveau huile'),
(uuid_generate_v4(), 'Verifier niveau lave glace');
