import { TestBed } from '@angular/core/testing';

import { NoteRestService } from './note-rest.service';

describe('NoteRestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NoteRestService = TestBed.get(NoteRestService);
    expect(service).toBeTruthy();
  });
});
