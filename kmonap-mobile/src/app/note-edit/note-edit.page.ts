import { Component, OnInit } from '@angular/core';
import { NoteRestService } from '../data/note-rest.service';
import { GuidService } from '../utils/guid.service';

@Component({
  selector: 'app-note-edit',
  templateUrl: './note-edit.page.html',
  styleUrls: ['./note-edit.page.scss'],
})
export class NoteEditPage implements OnInit {

  note:{ id:string, txt:string } = { id:null, txt:'A faire'};
  txt:string;
  constructor(
            private noteService:NoteRestService,
            private guidService:GuidService
          ) { }

  ngOnInit() {
  }

  public updateForm(data) {
    console.log(data);
    console.log(this.note);
    if(this.note.id == null) {
      this.note.id=this.guidService.newGuid();
      console.log(this.note.id);
      this.noteService.save(this.note).subscribe(
        (data) => { console.log("save OK"); }
      );
    } else {
      this.noteService.push(this.note).subscribe(
        (data) => { console.log("update OK"); }
      )
    }
  }
}
