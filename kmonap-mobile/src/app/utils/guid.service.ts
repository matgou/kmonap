import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * GuidService
 * Sercice to generate unique Guid
 *
 * Author: Mathieu GOULIN <mathieu.goulin@gadz.org>
 */
export class GuidService {

  constructor() {
    console.log('GuidService: constructor');
  }

  // http://stackoverflow.com/questions/26501688/a-typescript-guid-class
  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : ( r & 0x3 | 0x8 );
        return v.toString(16);
    });
  }
}
